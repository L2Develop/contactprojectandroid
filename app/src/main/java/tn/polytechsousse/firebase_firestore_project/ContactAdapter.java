package tn.polytechsousse.firebase_firestore_project;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.TextView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class ContactAdapter extends FirestoreRecyclerAdapter<Contact, ContactAdapter.ContactHolder> {

    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference contactBookref = db.collection("Contacts");
    private OnItemClickListener listener;
    private FirebaseAuth firebaseAuth;

    public ContactAdapter(@NonNull FirestoreRecyclerOptions<Contact> options) {
        super(options);
    }

    @Override
    protected void onBindViewHolder(@NonNull ContactHolder holder, int position, @NonNull Contact model) {
        System.out.println("CONTACT ID: "+model.getContactID());
        System.out.println("USER ID: "+firebaseAuth.getUid());
        if (model.getContactID().equals(firebaseAuth.getUid())) {
            holder.textViewName.setText(model.getName() + " " + model.getLastname());
            holder.btn_onelet.setText(model.getName().substring(0, 2));
            holder.textViewNumber.setText(model.getPhoneN());
            holder.adr_label.setText(model.getAdr());
        }else{
            holder.cardview.removeAllViews();
        }
    }

    public void deleteItem(int position) {
        getSnapshots().getSnapshot(position).getReference().delete();
    }


    @NonNull
    @Override
    public ContactHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.main_activity,
                parent, false);
        return new ContactHolder(v);
    }

    class ContactHolder extends RecyclerView.ViewHolder {
        TextView textViewName;
        TextView textViewNumber;
        TextView adr_label;
        Button btn_onelet;
        CardView cardview;


        public ContactHolder(View itemView) {
            super(itemView);
            textViewName = itemView.findViewById(R.id.contact_name);
            textViewNumber = itemView.findViewById(R.id.contact_phone);
            adr_label = itemView.findViewById(R.id.adr_label);
            btn_onelet = itemView.findViewById(R.id.btn_onelet);
            cardview = itemView.findViewById(R.id.cardview);
            firebaseAuth = FirebaseAuth.getInstance();

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION && listener != null) {
                        listener.onItemClick(getSnapshots().getSnapshot(position), position);
                    }
                }
            });
        }
    }

    public interface OnItemClickListener {
        void onItemClick(DocumentSnapshot documentSnapshot, int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }
}