package tn.polytechsousse.firebase_firestore_project;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;


public class Home extends AppCompatActivity {

    private static final String NAME_KEY = "Name";
    private static final String EMAIL_KEY = "Email";
    private static final String PHONE_KEY = "Phone";
    FirebaseFirestore db;
    TextView textDisplay;
    TextView message;
    EditText name, email, phone;
    Button save, read, update, realupdate, delete;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home);
        db = FirebaseFirestore.getInstance();
        textDisplay = findViewById(R.id.textDisplay);
        message = findViewById(R.id.displayMessage);
        save = findViewById(R.id.save);
        read = findViewById(R.id.read);
        update = findViewById(R.id.update);
        realupdate = findViewById(R.id.rtimupdate);
        delete = findViewById(R.id.delte);

        save.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                addNewContact();
            }
        });
    }

    private void addNewContact(){
        save = findViewById(R.id.save);
        name = findViewById(R.id.name);
        email= findViewById(R.id.email);
        phone = findViewById(R.id.phone);

        String mName = name.getText().toString();
        String mEmail = email.getText().toString();
        String mPhone = phone.getText().toString();
        Map<String, Object> newContact = new HashMap<>();
        newContact.put(NAME_KEY,mName);
        newContact.put(EMAIL_KEY,mEmail);
        newContact.put(PHONE_KEY,mPhone);
        db.collection("PhoneBook").document("Contacts").set(newContact)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(Home.this,"User REGISTERED",Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(Home.this,"ERROR" +e.toString(),Toast.LENGTH_SHORT).show();
                        Log.d("TAG",e.toString());
                    }
                });


    }
}
