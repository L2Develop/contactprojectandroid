package tn.polytechsousse.firebase_firestore_project;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

public class Register extends AppCompatActivity {

    FirebaseFirestore db;
    Button register,return_but;
    EditText emailfield, passwordField,confirmpasswordField;
    DocumentReference registerListener;
    FirebaseAuth firebaseAuth;
    ProgressBar pgsBar;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);
        db = FirebaseFirestore.getInstance();
        emailfield = findViewById(R.id.email_input);
        passwordField = findViewById(R.id.pw_input);
        confirmpasswordField = findViewById(R.id.confirm_input);
        register = findViewById(R.id.register_but);
        return_but = findViewById(R.id.return_but);
        pgsBar =  findViewById(R.id.pBar);
        firebaseAuth = FirebaseAuth.getInstance();
        register.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                registerFN();
            }
        });
        return_but.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Intent intent = new Intent(Register.this, LoginScreen.class);
                startActivity(intent);
            }
        });
    }

    private void registerFN() {
        System.out.println("Entered 1");
        String email, password, passwordconfirm;
        email = emailfield.getText().toString();
        password = passwordField.getText().toString();
        passwordconfirm = confirmpasswordField.getText().toString();

        if (TextUtils.isEmpty(email)) {
            Toast.makeText(getApplicationContext(), "Please enter email...", Toast.LENGTH_LONG).show();
            return;
        }
        if (TextUtils.isEmpty(password)) {
            Toast.makeText(getApplicationContext(), "Please enter password!", Toast.LENGTH_LONG).show();
            return;
        }

        if (!email.contains("@polytechnicien.tn"))
        {
            Toast.makeText(getApplicationContext(), "Verify your email please!", Toast.LENGTH_SHORT).show();
            return;
        }

        if (password.equals(passwordconfirm)) {
            pgsBar.setVisibility(View.VISIBLE);
            firebaseAuth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                pgsBar.setVisibility(View.GONE);
                                Toast.makeText(getApplicationContext(), "Registration successful!", Toast.LENGTH_LONG).show();

                                Intent intent = new Intent(Register.this, LoginScreen.class);
                                startActivity(intent);
                            }
                            else {
                                pgsBar.setVisibility(View.GONE);
                                Toast.makeText(getApplicationContext(), "Registration failed! Please try again later", Toast.LENGTH_LONG).show();
                            }
                        }
                    });
        }else
            Toast.makeText(getApplicationContext(),"Password doesn't match!",Toast.LENGTH_SHORT).show();
    }




}
