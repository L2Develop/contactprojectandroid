package tn.polytechsousse.firebase_firestore_project;

public class Contact {
    private String Name,Lastname,PhoneN,Adr,contactID;


    public Contact() {

}


    public Contact(String contactid,String name, String lastname, String phoneN, String adr){
        contactID = contactid;
        Name = name;
        Lastname = lastname;
        PhoneN = phoneN;
        Adr = adr;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getLastname() {
        return Lastname;
    }

    public void setLastname(String lastname) {
        Lastname = lastname;
    }

    public String getPhoneN() {
        return PhoneN;
    }

    public void setPhoneN(String phoneN) {
        PhoneN = phoneN;
    }

    public String getAdr() {
        return Adr;
    }

    public void setAdr(String adr) {
        Adr = adr;
    }

    public String getContactID() {
        return contactID;
    }

    public void setContactID(String contactID) {
        this.contactID = contactID;
    }
}
