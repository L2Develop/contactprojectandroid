package tn.polytechsousse.firebase_firestore_project;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;


import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;


public class Home_scr extends AppCompatActivity {


    private EditText editTextName;
    private EditText editTextLastName;
    private EditText editTextPhoneN;
    private EditText editTextAdr;
    private TextView textViewData;
    private Button add_button;
    private Button log_out;
    FirebaseAuth firebaseAuth;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference notebookRef = db.collection("Contacts");
    private DocumentSnapshot lastResult;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_scr);

        firebaseAuth = FirebaseAuth.getInstance();
        editTextName = findViewById(R.id.edit_text_name);
        editTextLastName = findViewById(R.id.edit_text_lastname);
        editTextPhoneN = findViewById(R.id.edit_text_phonen);
        editTextAdr = findViewById(R.id.edit_text_adr);
        textViewData = findViewById(R.id.text_view_data);
        add_button = findViewById(R.id.add_button);
        log_out = findViewById(R.id.logout);


    }

    @Override
    protected void onStart() {
        super.onStart();
        notebookRef.addSnapshotListener(this, new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(QuerySnapshot queryDocumentSnapshots, FirebaseFirestoreException e) {
                if (e != null) {
                    return;
                }

                String data = "";

                for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                    Contact contact = documentSnapshot.toObject(Contact.class);
                    String Name = contact.getName();
                    String LastName = contact.getLastname();
                    String PhoneN = contact.getPhoneN();
                    String Adr = contact.getAdr();


                    data +=
                             "\nName: " + Name + "\nLastName: " + LastName + "\nPhoneN: " + PhoneN
                            + "\nAdr: " + Adr +"\n";

                    if (queryDocumentSnapshots.size() > 0) {
                        data += "_________________________\n";
                        textViewData.append(data);

                        lastResult = queryDocumentSnapshots.getDocuments()
                                .get(queryDocumentSnapshots.size() - 1);
                    }

                    textViewData.setText(data);


                }



            }
        });

        add_button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                onAdd();
            }
        });

        log_out.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                onLogout();
            }
        });
    }

    public void onAdd(){
        Intent myIntent = new Intent(Home_scr.this, addContact.class);
        startActivityForResult(myIntent, 0);
    }

    public void onLogout(){
        firebaseAuth.signOut();
        Intent myIntent = new Intent(Home_scr.this, LoginScreen.class);
        startActivity(myIntent);
    }
//
//    public void loadNotes(View v) {
//        notebookRef.get()
//                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
//                    @Override
//                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
//                        String data = "";
//
//                        for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
//                            Contact contact = documentSnapshot.toObject(Contact.class);
//                            contact.setContactID(documentSnapshot.getId());
//
//                            String contactID = contact.getContactID();
//                            String name = contact.getName();
//                            String lastname = contact.getLastname();
//                            String phonen = contact.getPhoneN();
//                            String adr = contact.getAdr();
//
//                            data += "ID: " + contactID
//                                    + "\nName: " + name + "\nLastName: " + lastname + "\nPhoneN: " + phonen
//                                    + "\nAdr: " + adr +"\n\n";
//                        }
//
//                        textViewData.setText(data);
//                    }
//                });
//    }
}