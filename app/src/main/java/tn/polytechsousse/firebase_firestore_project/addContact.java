package tn.polytechsousse.firebase_firestore_project;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import android.widget.EditText;

import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;



import androidx.appcompat.app.AppCompatActivity;

public class addContact extends AppCompatActivity {

    private EditText editTextName;
    private EditText editTextLastName;
    private EditText editTextPhoneN;
    private EditText editTextAdr;
    FirebaseAuth firebaseAuth;

    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference contactBookRef = db.collection("Contacts");


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_contact);
        editTextName = findViewById(R.id.edit_text_name);
        editTextLastName = findViewById(R.id.edit_text_lastname);
        editTextPhoneN = findViewById(R.id.edit_text_phonen);
        editTextAdr = findViewById(R.id.edit_text_adr);
        firebaseAuth = FirebaseAuth.getInstance();


    }


      public void addContact(View v) {
        String name = editTextName.getText().toString();
        String lastname = editTextLastName.getText().toString();
        String phonen = editTextPhoneN.getText().toString();
        String adr = editTextAdr.getText().toString();

          if (name.matches("") || lastname.matches("") || phonen.matches("") || adr.matches(""))
          {
              Toast.makeText(addContact.this, "Please Verify the fields", Toast.LENGTH_SHORT).show();
              return;
          }

        Contact contact = new Contact(firebaseAuth.getUid(),name,lastname,phonen,adr);
        contactBookRef.add(contact);


          Intent myIntent = new Intent(addContact.this, MainActivity.class);
          startActivity(myIntent);
          Toast.makeText(addContact.this,"Contact Added",Toast.LENGTH_SHORT).show();
    }
}