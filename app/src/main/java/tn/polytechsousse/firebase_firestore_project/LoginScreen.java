package tn.polytechsousse.firebase_firestore_project;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

public class LoginScreen extends AppCompatActivity {

    FirebaseFirestore db;
    Button loginbut,regist_but;
    EditText phonefield, passwordField;
    DocumentReference loginListener;
    FirebaseAuth firebaseAuth;
    ProgressBar prgbar;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_screen);
        db = FirebaseFirestore.getInstance();
        phonefield = findViewById(R.id.phoneText);
        passwordField = findViewById(R.id.pwText);
        loginbut = findViewById(R.id.loginbut);
        regist_but = findViewById(R.id.regist_but);
        firebaseAuth = FirebaseAuth.getInstance();
        prgbar = findViewById(R.id.prgbar);
        loginbut.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Loginfn();
            }
        });

        regist_but.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Intent myIntent = new Intent(LoginScreen.this, Register.class);
                startActivityForResult(myIntent, 0);
            }
        });
    }

    private void Loginfn(){

        if (TextUtils.isEmpty(phonefield.getText())) {
            Toast.makeText(getApplicationContext(), "Please enter email...", Toast.LENGTH_LONG).show();
            return;
        }
        if (TextUtils.isEmpty(passwordField.getText())) {
            Toast.makeText(getApplicationContext(), "Please enter password...", Toast.LENGTH_LONG).show();
            return;
        }

        prgbar.setVisibility(View.VISIBLE);
        firebaseAuth.signInWithEmailAndPassword(phonefield.getText().toString(),passwordField.getText().toString()).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()){
                    Intent myIntent = new Intent(LoginScreen.this, MainActivity.class);
                    startActivityForResult(myIntent, 0);
                    prgbar.setVisibility(View.GONE);
                    Toast.makeText(LoginScreen.this,"Sign in successful",Toast.LENGTH_SHORT).show();
                }else {
                    prgbar.setVisibility(View.GONE);
                    Toast.makeText(LoginScreen.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }




}
