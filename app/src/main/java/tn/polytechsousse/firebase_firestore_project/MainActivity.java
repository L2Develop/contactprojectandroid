package tn.polytechsousse.firebase_firestore_project;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class MainActivity extends AppCompatActivity {
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference contactBookref = db.collection("Contacts");

    private ContactAdapter adapter;
    private FloatingActionButton addContact,log_out;
    FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity1);
        addContact = findViewById(R.id.addContact);
        log_out = findViewById(R.id.log_out);
        firebaseAuth = FirebaseAuth.getInstance();
        setUpRecyclerView();
    }

    private void setUpRecyclerView() {
        Query query = contactBookref.orderBy("name", Query.Direction.ASCENDING);
        FirestoreRecyclerOptions<Contact> options = new FirestoreRecyclerOptions.Builder<Contact>()
                .setQuery(query, Contact.class)
                .build();

        adapter = new ContactAdapter(options);

        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0,
                ItemTouchHelper.LEFT ) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(final RecyclerView.ViewHolder viewHolder, int direction) {
                new AlertDialog.Builder(MainActivity.this)
                        .setTitle("Delete Contact")
                        .setMessage("Are you sure you want to delete this contact?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        adapter.deleteItem(viewHolder.getAdapterPosition());
                        Toast.makeText(MainActivity.this, "Contact Deleted", Toast.LENGTH_SHORT).show();
                    }
                })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                adapter.notifyItemChanged(viewHolder.getAdapterPosition());
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();

            }
        }).attachToRecyclerView(recyclerView);


        adapter.setOnItemClickListener(new ContactAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(DocumentSnapshot documentSnapshot, int position) {
                Contact contact = documentSnapshot.toObject(Contact.class);
                String id = documentSnapshot.getId();
                String name = contact.getName();
                String lastname = contact.getLastname();
                String adr = contact.getAdr();
                String phone = contact.getPhoneN();
                Intent aa = new Intent(MainActivity.this,updateContact.class);

                aa.putExtra("name",name);
                aa.putExtra("lastname",lastname);
                aa.putExtra("phone",phone);
                aa.putExtra("adr",adr);
                aa.putExtra("id",id);
                startActivity(aa);
            }
        });
    }


    @Override
    protected void onStart() {
        super.onStart();
        adapter.startListening();

        addContact.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                onAdd();
            }
        });

        log_out.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                onLogout();
            }
        });
    }



    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();
    }

    public void onLogout(){
        firebaseAuth.signOut();
        Toast.makeText(this, "Signed out", Toast.LENGTH_SHORT).show();
        Intent myIntent = new Intent(MainActivity.this, LoginScreen.class);
        startActivity(myIntent);
        finish();
    }

    public void onAdd(){
        Intent myIntent = new Intent(MainActivity.this, addContact.class);
        startActivityForResult(myIntent, 0);
    }
}