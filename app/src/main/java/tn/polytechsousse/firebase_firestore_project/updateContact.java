package tn.polytechsousse.firebase_firestore_project;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import android.widget.Button;
import android.widget.EditText;

import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;



import androidx.appcompat.app.AppCompatActivity;

public class updateContact extends AppCompatActivity {

    private EditText name_label;
    private EditText lastname_label;
    private EditText phone_label;
    private EditText adr_label;
    private Button update_btn;
    FirebaseAuth firebaseAuth;

    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference contactBookRef = db.collection("Contacts");


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.update_contact);
        name_label = findViewById(R.id.name_label);
        lastname_label = findViewById(R.id.lastname_label);
        phone_label = findViewById(R.id.phone_label);
        adr_label = findViewById(R.id.adr_label);
        update_btn = findViewById(R.id.update_btn);

        String name = getIntent().getExtras().getString("name");
        String lastname = getIntent().getExtras().getString("lastname");
        String adr = getIntent().getExtras().getString("adr");
        String phone = getIntent().getExtras().getString("phone");


        name_label.setText(name);
        lastname_label.setText(lastname);
        adr_label.setText(adr);
        phone_label.setText(phone);

        update_btn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                updateContact();
            }
        });


    }


    public void updateContact() {


        String id = getIntent().getExtras().getString("id");

        String name_lab = name_label.getText().toString();
        String last_lab = lastname_label.getText().toString();
        String adr_lab = adr_label.getText().toString();
        String phone_lab = phone_label.getText().toString();

        if (name_lab.matches("") || last_lab.matches("") || adr_lab.matches("") || phone_lab.matches(""))
        {
            Toast.makeText(updateContact.this, "Please Verify the fields", Toast.LENGTH_SHORT).show();
            return;
        }

        contactBookRef.document(id).update("name",name_lab);
        contactBookRef.document(id).update("lastname",last_lab);
        contactBookRef.document(id).update("adr",adr_lab);
        contactBookRef.document(id).update("phoneN",phone_lab);


        Intent myIntent = new Intent(updateContact.this, MainActivity.class);
        startActivity(myIntent);
        finish();
//        Toast.makeText(addContact.this,"Contact Added",Toast.LENGTH_SHORT).show();
    }
}